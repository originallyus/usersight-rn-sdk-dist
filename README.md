# React Native UserSightSDK

A cross-platform user feedback component for React Native.

## Demo App

- Demo app is available from [here](https://by.originally.us/beta/usersight-rn)

## 🚀 Getting Started

**React Native 0.60 and higher**
**

```bash
yarn add usersight-rn-sdk # or npm install --save usersight-rn-sdk
```

Then rebuild your app. On iOS, be sure to `pod install` your cocoapods in your app's `/ios` directory.

**React Native 0.59 and lower**

Using [`yarn`]:

```bash
yarn add usersight-rn-sdk
react-native link usersight-rn-sdk
```

Now we need to install peer dependencies modules: [`axios`](https://github.com/axios/axios), [`crypto-js`](https://github.com/brix/crypto-js),[`react-native-device-info`](https://github.com/react-native-community/react-native-device-info), [`async-storage`](https://github.com/emeraldsanto/react-native-encrypted-storage),  [`react-native-rate`](https://github.com/KjellConnelly/react-native-rate) and [`react-native-root-siblings`](https://github.com/magicismight/react-native-root-siblings). You can skip this if your host application is already using them.

Run the following:

```sh
yarn add axios crypto-js react-native-device-info react-native-encrypted-storage react-native-rate react-native-root-siblings
```

Next, we need to link these libraries. The steps depends on your React Native version:

- **React Native 0.60 and higher**

  On newer versions of React Native, [linking is automatic](https://github.com/react-native-community/cli/blob/master/docs/autolinking.md).

  To complete the linking on iOS, make sure you have [Cocoapods](https://cocoapods.org/) installed. Then run:

  ```sh
  cd ios
  pod install
  cd ..
  ```

- **React Native 0.59 and lower**

  If you're on an older React Native version, you need to manually link the dependencies. To do that, check Manual installation on github repo:
  [`axios`](https://github.com/axios/axios)
  [`react-native-device-info`](https://github.com/react-native-community/react-native-device-info)
  [`async-storage`](https://github.com/emeraldsanto/react-native-encrypted-storage)
  [`crypto-js`](https://github.com/brix/crypto-js)
  [`react-native-rate`](https://github.com/KjellConnelly/react-native-rate)

We're done! Now you can build and run the sample app on your device/simulator.

## Quick Start

Please initalize the SDK once in App.js. The mounting point of the SDK should be placed directly in your App.js for it to stay on top of all other views.

```js
import UserSightSDK from 'usersight-rn-sdk';

const App = (props) => {
  // This initialization should done once inside App.js
  const userSightRef = useRef();
};

// in your render function 
return (
  // Make sure the app is full screen and vertical align center
  <View style={{ flex: 1, justifyContent: 'center' }}>
    <YourOtherContainerView></YourOtherContainerView>

    {/* Please insert this mount point directly in your App.js,
        preferably at the bottom of your root view tree for it to stay on top of all other views */}
    <UserSightSDK ref={userSightRef} />
  </View>
);
```

All other functions to be invoked on userSightRef.current can be done anywhere in your application code (can also be inside App.js)

```js
import UserSightSDK from 'usersight-rn-sdk';

const OtherComponent = (props) => {
  // This is a singleton. The initialization should have already been done inside App.js
  const userSightRef = useRef();

  const setupFeedbackSDK = () => {
    // Please contact us to obtain an App Secret specific to your Bundle ID/Package ID
    userSightRef.current.setAppSecret('Vlx6fTAHZ6vbZI8Pmj07');
  };

  const setLanguage = (language) => {
    // Use international language codes: "en", "zh", "zh_tw", "ms", "ta", "id", "th", "tl", "vi"
    // Note: our backend must be loaded with the translation content before a language can be used. Invalid languages will automatically fallback to English
    userSightRef.current.setLanguage(language);
  };

  const setAppUserId = () => {
    // This is an optional User ID provided by the host app.
    userSightRef.current.setAppUserId('a987654321z');
  };

  const setMetadata = () => {
    // This is an optional metadata provided by the host app. Supports string format only.
    userSightRef.current.setMetadata('trail_1234567890');
  };

  const testRatingForm = (eventTag) => {
    // This will always show the Feedback form for testing/debugging purpose
    // You may troubleshoot this in "Request (Testing)" section in CMS
    userSightRef.current.setFormSlug('native_rating_form');
    userSightRef.current.testRatingForm(eventTag);
  };

  const showRatingForm = (eventTag) => {
    // This may not always show/trigger the form, depending on the Response Frequency & Prompt Frequency configuration on backend
    // You may troubleshoot this in "Request" section in CMS
    userSightRef.current.showRatingForm(eventTag);
  };
};
```


## Callback Function (optional)

An optional callback function can be provided as parameter to capture the outcome of the SDK.

  - **formDidShow = false** is triggered when there is no form to be shown, this callback function will be triggered almost immediately.
  - **formDidShow = true** is triggered when has been shown and user has finished interacting with it or dismiss/cancel it

```js
userSightRef.current.testRatingForm(eventTag, (formDidShow) => {
  console.log("formDidShow", formDidShow);
});

userSightRef.current.showRatingForm(eventTag, (formDidShow) => {
  console.log("formDidShow", formDidShow);
});
```