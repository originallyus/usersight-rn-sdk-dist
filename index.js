import React from "react";
import {
  Dimensions,
  Animated,
  Keyboard,
  Easing,
  Linking,
  Platform,
  View,
  KeyboardAvoidingView,
} from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import Rate, { AndroidMarket } from "react-native-rate";
import { ContentUI } from "./src/component";
import { sendRequest, sendSubmit } from "./src/api";
import {
  heightScreen,
  isTablet,
  widthScreen,
  widthScreenNonIntr,
  XVersion,
} from "./src/utils/const";
import { getStatusBarHeight } from "./src/utils/StatusBarHeight";
import * as _ from './src/utils/MyFunction'

const { height, width } = Dimensions.get("window");

// milliseconds
let requestFormTimestamp = null;
let submitFormTimestamp = null;

class UserSightSDK extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {},
      originalFormModel: {},
      numStar: 0,
      inputValue: "",
      loading: false,
      listActiveSelect: [],
      isVisible: false,
      didSubmitted: false,
      isShowingExtraForm: false,
      heightScreenTablet: "auto",
    };
    this.animatedValue = new Animated.Value(0);
    this.keyboardHeight = new Animated.Value(0);
    this.formSlug = "native_rating_form";
    this.scrollViewInputRef = null;
    this.isShowingKeyboard = false;
    this.timeout = null;
    this.formDidShow = true;
    this.eventTag = null;
    this.showKeyboardSub = null;
    this.hideKeyboardSub = null;
    this.heightParentView = 0;
    this.isVisibleValue = new Animated.Value(0);
    this.opacityAnimatedValue = new Animated.Value(1);

    // user-provided parameters
    this.metadata = null;
    this.sdkCallbackFunction = null;
    this.appUserId = '',
      this.appSecret = '',
      this.language = 'en';
  }

  componentDidUpdate(prevProps, prevState) {
    const { isVisible } = this.state;
    if (isVisible !== prevState.isVisible) {
      if (isVisible) {
        Animated.timing(this.isVisibleValue, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }
    }
  }

  //----------------------------------------------------------------------------------------
  // Keyboard handling
  //----------------------------------------------------------------------------------------

  onKeyboardDidShow = (e) => {
    const { form } = this.state;
    this.isShowingKeyboard = true;

    let customKeyboardHeight = e.endCoordinates.height
    if (isTablet && form?.instrusive_style)
      customKeyboardHeight = e.endCoordinates.height;

    // only android mobile
    if (!isTablet && form?.instrusive_style && Platform.OS === "android")
      customKeyboardHeight = 1;

    Animated.timing(this.keyboardHeight, {
      toValue: customKeyboardHeight,
      duration: 300,
      easing: Easing.ease,
      useNativeDriver: false,
    }).start(() => {
      if (this.scrollViewInputRef) {
        this.scrollViewInputRef.scrollToEnd({ duration: 350, animated: true });
      }
    });
  };

  onKeyboardDidHide = () => {
    this.isShowingKeyboard = false;
    Animated.timing(this.keyboardHeight, {
      toValue: 0,
      duration: 350,
      useNativeDriver: false,
    }).start();
  };

  componentDidMount() {
    if (Platform.OS === "ios") {
      this.showKeyboardSub = Keyboard.addListener("keyboardWillShow", this.onKeyboardDidShow);
      this.hideKeyboardSub = Keyboard.addListener("keyboardWillHide", this.onKeyboardDidHide);
      return;
    }
    // no animation with Keyboard android
    this.showKeyboardSub = Keyboard.addListener("keyboardDidShow", this.onKeyboardDidShow);
    this.hideKeyboardSub = Keyboard.addListener("keyboardDidHide", this.onKeyboardDidHide);
  }

  componentWillUnmount() {
    try {
      if (this.timeout) clearTimeout(this.timeout);

      if (this.showKeyboardSub?.remove && this.hideKeyboardSub?.remove) {
        this.showKeyboardSub.remove()
        this.hideKeyboardSub.remove()
        return;
      }

      if (Platform.OS === "ios") {
        Keyboard.removeListener("keyboardWillShow", this.onKeyboardDidShow);
        Keyboard.removeListener("keyboardWillHide", this.onKeyboardDidHide);
        return;
      }

      // no animation with Keyboard android
      Keyboard.removeListener("keyboardDidShow", this.onKeyboardDidShow);
      Keyboard.removeListener("keyboardDidHide", this.onKeyboardDidHide);
    } catch (error) {
      console.log("UserSightSDK Error:", error);
    }
  }

  //----------------------------------------------------------------------------------------
  // Public functions
  //----------------------------------------------------------------------------------------
  getVersion = () => {
    return XVersion
  };

  setFormSlug = (formSlug) => {
    // sanity check
    if (!formSlug) return;
    if (formSlug.trim().length < 2) return;

    this.formSlug = formSlug.trim();
  };

  setAppSecret = (appSecret) => {
    this.appSecret = appSecret;
  };

  setAppUserId = (userId = '') => {
    try {
      if (typeof userId === 'object' && userId !== null) {
        const userIdObject =
          Object.keys(userId).length === 0 ? '' : JSON.stringify(userId);
        this.appUserId = userIdObject;
        return;
      }

      const userIdString = userId ? userId.toString() : '';
      this.appUserId = userIdString;
    } catch (error) {
      console.log('UserSightSDK setAppUserId error:', error);
      this.appUserId = '';
    }
  };

  setLanguage = (language) => {
    // sanity check
    if (!language || language.length < 2) {
      language = 'en';
    }

    language = language.toLowerCase();
    this.language = language;
  };

  setMetadata = (metadata) => {
    if (!metadata) return;
    this.metadata = { metadata };
  };

  testRatingForm = (eventTag, callbackFn) => {
    this.requestForm(true, eventTag, callbackFn);
  };

  showRatingForm = (eventTag, callbackFn) => {
    this.requestForm(false, eventTag, callbackFn);
  };

  /*
   * Forcefully dismiss the rating UI
   * This may be suitable for scenarios like session timeout, session expired where host application
   * needs to forcefully dismiss any user-related UI and logout the user
   * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
   * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this feature
   */
  hideRatingForm = () => {
    this.setState({
      form: {},
      originalFormModel: {},
      numStar: 0,
      inputValue: "",
      loading: false,
      listActiveSelect: [],
      isVisible: false,
      didSubmitted: false,
      isShowingExtraForm: false,
      heightScreenTablet: "auto",
    });

    this.performCallbackFunction();

    // reset
    this.sdkCallbackFunction = null;
    this.animatedValue = new Animated.Value(0);
    this.isShowingKeyboard = false;
    this.keyboardHeight = new Animated.Value(0);
  };

  performCallbackFunction = () => {
    if (this.sdkCallbackFunction && typeof this.sdkCallbackFunction === 'function') {
      this.sdkCallbackFunction(this.formDidShow);
    }

    this.sdkCallbackFunction = null;
  };


  //----------------------------------------------------------------------------------------
  // Request form implementation
  //----------------------------------------------------------------------------------------

  requestForm = async (debug, eventTag = "", callbackFn = null) => {
    // check isShowing
    if (this.state.isVisible) return;

    // save callback function, if any
    this.formDidShow = false;
    this.sdkCallbackFunction = null;
    if (callbackFn && typeof callbackFn === "function") {
      this.sdkCallbackFunction = callbackFn;
    }

    // debounce: prevent the SDK to be called too fast (press test buttons very quickly on after another)
    if (requestFormTimestamp) {
      const nowTimestamp = Date.now();
      if (nowTimestamp - requestFormTimestamp < 3000) return;
    }
    requestFormTimestamp = Date.now();

    // reset, in case there's errors earlier
    this.setState({
      form: {},
      originalFormModel: {},
      numStar: 0,
      inputValue: "",
      loading: false,
      listActiveSelect: [],
      didSubmitted: false,
      isShowingExtraForm: false,
    });

    // latch these values
    this.eventTag = eventTag;
    this.debug = debug ? 1 : 0;   //use numeric better for backend handling

    // check install_timestamp
    // Note: this is saved in keychain, hence will persist across re-installation
    // We're working around this by combining with DeviceInfo.getBuildNumber()
    const build_number = await _.getBuildNumber();
    const storage_key = `dk3@sq8_${build_number}`;
    const install_timestamp = await EncryptedStorage.getItem(storage_key);
    if (!install_timestamp)
      EncryptedStorage.setItem(storage_key, `${Math.floor(Date.now() / 1000)}`);

    //For testing only
    //console.log("build_number:", build_number);
    //console.log("install_timestamp:", install_timestamp);
    //install_timestamp = 0;

    // request form data
    const bodyData = {
      slug: this.formSlug,
      debug,
      event_tag: eventTag,
      install_timestamp: install_timestamp || 0,
      metadata: this.metadata || null,
    };

    // network request
    const headers = {
      'X-App-Secret': this.appSecret,
      'X-App-User-Id': this.appUserId,
      'X-Lang': this.language
    }

    const res = await sendRequest(bodyData, { headers });
    if (res?.error) {
      // critical error, show directly to UI
      if (debug)
        Alert.alert("UserSightSDK Error", res?.error, [{ text: "OK" }], { cancelable: true });
      this.performCallbackFunction();
      return;
    }

    // check for valid model data
    if (!res?.title || !res?.theme) {
      this.performCallbackFunction();
      return;
    }

    // indicate the form was shown
    this.formDidShow = true;

    // show the form
    this.setState({ form: res, isVisible: true }, () => {
      setTimeout(() => {
        Animated.timing(this.animatedValue, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }, 350);
    });
  };

  //----------------------------------------------------------------------------------------
  // Events
  //----------------------------------------------------------------------------------------

  handleButtonClose = () => {
    Animated.timing(this.isVisibleValue, {
      toValue: 0,
      useNativeDriver: false,
    }).start(() => {
      this.hideRatingForm();
    });
  };

  handleChangeText = (value) => {
    this.setState({
      inputValue: value,
    });
  };

  handleMultiSelectOption = (index) => {
    const { listActiveSelect } = this.state;
    const indexActive = listActiveSelect.indexOf(index);
    if (indexActive === -1) {
      listActiveSelect.push(index);
      this.setState({
        listActiveSelect: [...listActiveSelect],
      });
    } else {
      this.setState({
        listActiveSelect: listActiveSelect.filter(
          (element, i) => element !== index
        ),
      });
    }
  };

  handleSelectOption = (index) => {
    this.setState({
      listActiveSelect: [index],
    });
  };

  // used for both start & number rating
  handlePressRating = (numberValue) => {
    this.setState({ numStar: numberValue }, () => {
      if (
        this.state.form?.auto_submit &&
        this.state.form?.auto_submit?.length &&
        this.state.form?.auto_submit?.includes(numberValue)
      ) {
        this.submitForm(true);
      }
    });
  };

  //----------------------------------------------------------------------------------------
  // Helper functions
  //----------------------------------------------------------------------------------------

  blinkAnimation = () => {
    Animated.sequence([
      Animated.timing(this.opacityAnimatedValue, {
        toValue: 0,
        duration: 350,
        useNativeDriver: false,
      }),
      Animated.timing(this.opacityAnimatedValue, {
        toValue: 1,
        duration: 350,
        useNativeDriver: false,
      }),
    ]).start();
  };

  //----------------------------------------------------------------------------------------
  // Submit form implementation
  //----------------------------------------------------------------------------------------

  submitForm = async (isAutoSubmit = false) => {
    const {
      numStar,
      inputValue,
      listActiveSelect,
      form,
      isShowingExtraForm,
      originalFormModel,
      didSubmitted,
    } = this.state;

    // debounce: prevent user from pressing the Submit button repeatedly
    if (submitFormTimestamp) {
      const nowTimestamp = Date.now();
      if (nowTimestamp - submitFormTimestamp < 1000) return;
    }
    submitFormTimestamp = Date.now();

    // open external URL, if any
    if (form?.url && form?.url?.length > 5) {
      Linking.canOpenURL(form?.url).then((supported) => { supported && Linking.openURL(form?.url) },
        (err) => console.log("UserSightSDK Error:", err));
      //no return here
    }

    // we're NOT in auto store view mode, redirect to app store review if store review URL exists
    if (form && !form.auto_store_review) {
      const url = Platform.OS === "ios" ? form?.ios_review_url : form?.android_review_url;
      if (url) {
        Linking.canOpenURL(url).then((supported) => { supported && Linking.openURL(url) },
          (err) => console.log("UserSightSDK Error:", err));
      }
      //no return here
    }

    // we're showing intermmediate form, click Submit should reload UI using original model
    if (isShowingExtraForm) {
      this.handleSubmitFormResponse(originalFormModel);
      return;
    }

    // already submitted, nothing to do after this
    if (didSubmitted) {
      this.handleButtonClose();
      return;
    }

    // show loading indicator
    this.setState({ loading: true });

    // prepare data for API
    const bodyData = {
      slug: this.formSlug,
      rating: numStar,
      event_tag: this.eventTag,
      debug: this.debug,
      request_id: form?.id,
      selected_options: listActiveSelect.join(","),
      free_text: inputValue,
      auto: isAutoSubmit,
    };

    // initialize install_timestamp, if any
    const install_timestamp = await EncryptedStorage.getItem("@opkjnbvfdxc");
    if (!install_timestamp)
      await EncryptedStorage.setItem(
        "@opkjnbvfdxc",
        `${Math.floor(Date.now() / 1000)}`
      );

    // network request
    const headers = {
      'X-App-Secret': this.appSecret,
      'X-App-User-Id': this.appUserId,
      'X-Lang': this.language
    }

    const res = await sendSubmit(bodyData, { headers });
    if (res?.error) {
      // critical error, show directly to UI
      Alert.alert("UserSightSDK Error", res?.error, [{ text: "OK" }], { cancelable: true });

      //make sure to hide form when API fails
      this.hideRatingForm();
      return;
    }

    // flag to indicate we have call /submit API
    this.setState({ didSubmitted: true });

    // hide loading indicator
    this.setState({ loading: false });

    // auto show native Rating review UI
    // Note: this can only be shown about 3 times/year, this may fail (shows nothing) but doesn't matter
    if (res?.auto_store_review) {
      const options = {
        AppleAppID: `${res?.ios_store_app_id}`,
        OtherAndroidURL: res?.android_review_url,
        preferredAndroidMarket: AndroidMarket.Google,
        preferInApp: true,
        openAppStoreIfInAppFails: false,
      };
      Rate.rate(options, (success) => {
        console.log("success", success);
      });
      //no return here
    }

    // check for intermmediate form UI to be shown
    if (res?.extra_form) {
      this.handleExtraFormResponse(res);
      return;
    }

    // handle normal Success UI
    this.handleSubmitFormResponse(res);
    return;
  };

  // handle intermmediate form UI
  handleExtraFormResponse = (res) => {
    this.setState(
      {
        isShowingExtraForm: true,
      },
      () => {
        this.blinkAnimation();
        setTimeout(() => {
          this.setState({
            originalFormModel: res,
            form: res.extra_form,
          });
        }, 350);
      }
    );
  };

  // handle normal Success UI
  handleSubmitFormResponse = (res) => {
    this.setState(
      {
        isShowingExtraForm: false,
      },
      () => {
        this.blinkAnimation();
        setTimeout(() => {
          this.setState({
            originalFormModel: null,
            form: res,
          });
        }, 350);
      }
    );
  };

  //----------------------------------------------------------------------------------------
  // Render
  //----------------------------------------------------------------------------------------

  render() {
    const {
      form,
      numStar,
      inputValue,
      listActiveSelect,
      didSubmitted,
      loading,
      isShowingExtraForm,
      heightScreenTablet,
    } = this.state;

    const { theme } = form;

    const heightParentViewAni = this.isVisibleValue.interpolate({
      inputRange: [0, 1],
      outputRange: [height, 0],
    });

    const animaginSelectBtn = this.keyboardHeight.interpolate({
      inputRange: [0, 200],
      outputRange: [(16 / 667) * heightScreen, (10 / 667) * heightScreen],
    });

    const cmtTranslateY = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [150, 0],
    });

    const cmtAniOpacity = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });

    const opacityUI = this.opacityAnimatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });

    let widthStyle = widthScreen;
    let translateX = -(widthStyle / 2);
    let bottomStyle = "auto";
    let rightStyle = "auto";
    let leftStyle = '50%';
    if (!!!form.instrusive_style) {
      bottomStyle = 0;
    }

    if (!!!form.instrusive_style && isTablet) {
      widthStyle = widthScreenNonIntr;
      rightStyle = "15%";
      translateX = 0
      leftStyle = "auto";
    }

    let heightUI;
    if (isTablet) heightUI = heightScreenTablet;
    else {
      heightUI = heightScreenTablet === "auto" ? "auto" : heightScreen - 40;
    }

    let heightNonIntrusCondition = heightScreen - 40;
    if (isTablet) heightNonIntrusCondition = heightScreen / 1.5;

    // Animation
    let animationOutSideApp = 0
    let animationInSideApp = 0
    // animation for tablet, ios mobile with instrusive_style = 0
    if (isTablet || (!isTablet && !form.instrusive_style && Platform.OS !== 'android'))
      animationOutSideApp = this.keyboardHeight

    // animation mobile with instrusive_style = 1
    if (!isTablet && form.instrusive_style)
      animationInSideApp = this.keyboardHeight

    return (
      <Animated.View
        style={{
          position: "absolute",
          transform: [{ translateY: heightParentViewAni }, { translateX }],
          paddingBottom: animationOutSideApp,
          width: widthStyle,
          bottom: bottomStyle,
          right: rightStyle,
          left: leftStyle,
          zIndex: 9999999,    // for ios
          elevation: 9999999, // for android
        }}
      >
        {JSON.stringify(form) !== JSON.stringify({}) &&
          !!form.instrusive_style && Platform.OS === 'android' && (
            <KeyboardAvoidingView behavior={"height"}
              style={{
                backgroundColor: theme?.body_bg_color || "#fff",
                borderRadius: isTablet ? 8 : 0,
                flex: 1,
              }}
            >
              <Animated.View
                onLayout={(event) => {
                  var { height } = event.nativeEvent.layout;
                  if (isTablet) return

                  if (height > heightScreen && heightScreenTablet === "auto") {
                    this.setState({ heightScreenTablet: heightScreen });
                  }
                }}
                style={[
                  {
                    flex: 1,
                    height: isTablet ? heightScreenTablet : heightScreen,
                    opacity: opacityUI,
                    paddingBottom: isTablet ? 35 : getStatusBarHeight(),
                  },
                ]}
              >
                <ContentUI
                  didSubmitted={didSubmitted}
                  form={form}
                  animatedValues={{
                    keyboardHeight: this.keyboardHeight,
                  }}
                  animations={{
                    animaginSelectBtn: animaginSelectBtn,
                    cmtTranslateY: cmtTranslateY,
                    cmtAniOpacity: cmtAniOpacity,
                    animationInSideApp: animationInSideApp,
                  }}
                  states={{
                    inputValue: inputValue,
                    numStar: numStar,
                    listActiveSelect: listActiveSelect,
                    loading,
                    isShowingExtraForm,
                  }}
                  functions={{
                    submitForm: this.submitForm,
                    handlePressRating: this.handlePressRating,
                    handleButtonClose: this.handleButtonClose,
                    handleChangeText: this.handleChangeText,
                    handleSelectOption: this.handleSelectOption,
                    handleMultiSelectOption: this.handleMultiSelectOption,
                    scrollViewInputRef: (ref) =>
                      (this.scrollViewInputRef = ref),
                  }}
                />
              </Animated.View>
            </KeyboardAvoidingView>
          )}
        {JSON.stringify(form) !== JSON.stringify({}) &&
          !!form.instrusive_style && Platform.OS === 'ios' && (
            <View
              style={{
                backgroundColor: theme?.body_bg_color || "#fff",
                borderRadius: isTablet ? 8 : 0,
                flex: 1,
              }}
            >
              <Animated.View
                onLayout={(event) => {
                  var { height } = event.nativeEvent.layout;
                  if (isTablet) return

                  if (height > heightScreen && heightScreenTablet === "auto") {
                    this.setState({ heightScreenTablet: heightScreen });
                  }
                }}
                style={[
                  {
                    flex: 1,
                    height: isTablet ? heightScreenTablet : heightScreen,
                    opacity: opacityUI,
                    paddingBottom: isTablet ? 35 : getStatusBarHeight(),
                  },
                ]}
              >
                <ContentUI
                  didSubmitted={didSubmitted}
                  form={form}
                  animatedValues={{
                    keyboardHeight: this.keyboardHeight,
                  }}
                  animations={{
                    animaginSelectBtn: animaginSelectBtn,
                    cmtTranslateY: cmtTranslateY,
                    cmtAniOpacity: cmtAniOpacity,
                    animationInSideApp: animationInSideApp,
                  }}
                  states={{
                    inputValue: inputValue,
                    numStar: numStar,
                    listActiveSelect: listActiveSelect,
                    loading,
                    isShowingExtraForm,
                  }}
                  functions={{
                    submitForm: this.submitForm,
                    handlePressRating: this.handlePressRating,
                    handleButtonClose: this.handleButtonClose,
                    handleChangeText: this.handleChangeText,
                    handleSelectOption: this.handleSelectOption,
                    handleMultiSelectOption: this.handleMultiSelectOption,
                    scrollViewInputRef: (ref) =>
                      (this.scrollViewInputRef = ref),
                  }}
                />
              </Animated.View>
            </View>
          )}
        {JSON.stringify(form) !== JSON.stringify({}) &&
          !!!form.instrusive_style && (
            <View
              style={[
                {
                  borderTopLeftRadius: (16 / 375) * widthScreenNonIntr,
                  borderTopRightRadius: (16 / 375) * widthScreenNonIntr,
                  backgroundColor: theme?.body_bg_color || "#fff",
                },
              ]}
            >
              <Animated.View
                onLayout={(event) => {
                  var { height } = event.nativeEvent.layout;
                  if (isTablet) return
                  if (
                    height > heightNonIntrusCondition &&
                    heightScreenTablet === "auto"
                  ) {
                    this.setState({
                      heightScreenTablet: heightNonIntrusCondition,
                    });
                  }
                }}
                style={[
                  {
                    flex: 1,
                    height: heightUI,
                    opacity: opacityUI,
                    paddingBottom: getStatusBarHeight(),
                  },
                ]}
              >
                <ContentUI
                  didSubmitted={didSubmitted}
                  form={form}
                  animatedValues={{
                    keyboardHeight: this.keyboardHeight,
                  }}
                  animations={{
                    animaginSelectBtn: animaginSelectBtn,
                    cmtTranslateY: cmtTranslateY,
                    cmtAniOpacity: cmtAniOpacity,
                    animationInSideApp: animationInSideApp,
                  }}
                  states={{
                    inputValue: inputValue,
                    numStar: numStar,
                    listActiveSelect: listActiveSelect,
                    loading,
                    isShowingExtraForm,
                  }}
                  functions={{
                    submitForm: this.submitForm,
                    handlePressRating: this.handlePressRating,
                    handleButtonClose: this.handleButtonClose,
                    handleChangeText: this.handleChangeText,
                    handleSelectOption: this.handleSelectOption,
                    handleMultiSelectOption: this.handleMultiSelectOption,
                    scrollViewInputRef: (ref) =>
                      (this.scrollViewInputRef = ref),
                  }}
                />
              </Animated.View>
            </View>
          )}
      </Animated.View>
    );
  }
}

export default UserSightSDK;