import { Dimensions } from 'react-native';
import DeviceInfo from 'react-native-device-info';

const { width, height } = Dimensions.get('window');
const isTablet = typeof DeviceInfo.isTablet() === 'object' ? false : DeviceInfo.isTablet();

const widthScreen = isTablet ? 768 : width;
const heightScreen = isTablet ? (height * 3) / 4 : height;

const widthScreenNonIntr = isTablet ? 375 : width;
const heightScreenNonIntr = isTablet ? 375 : height;

const fontSizePlus = 6;

let mainPaddingVertical = (16 / 667) * heightScreen;
let mainPaddingHorizontal = (21 / 375) * widthScreen;

if (height <= 568) {
  mainPaddingVertical = (10 / 667) * height;
}
if (width <= 320) {
  mainPaddingHorizontal = (14 / 375) * width;
}

const shuffle = array => {
  let currentIndex = array.length;
  let temporaryValue;
  let randomIndex;
  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
};

export {
  mainPaddingHorizontal,
  mainPaddingVertical,
  widthScreen,
  heightScreen,
  widthScreenNonIntr,
  heightScreenNonIntr,
  isTablet,
  fontSizePlus,
  shuffle,
};

export const Fonts = {
  CondensedMedium: {
    fontFamily: 'AIAEverest-CondensedMedium',
  },
  Medium: {
    fontFamily: 'AIAEverest-Medium',
  },
  Regular: {
    fontFamily: 'AIAEverest-Regular',
  },
  Bold: {
    fontFamily: 'AIAEverestBold',
  },
};

export const XVersion = '20220509';
