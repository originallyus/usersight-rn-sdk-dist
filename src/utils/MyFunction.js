

import { Dimensions } from 'react-native';
import DeviceInfo from 'react-native-device-info';

const isTablet = typeof DeviceInfo.isTablet() === 'object' ? false : DeviceInfo.isTablet();
const {width, height} = Dimensions.get('window');

export function isObject(value) {
  const type = typeof value
  return value != null && (type === 'object' || type === 'function')
}

export function sample(array) {
  const length = array == null ? 0 : array.length
  const timestampSeconds = Math.floor(Date.now() / 1000);
  return length ? array[timestampSeconds % length] : undefined
}

export function isNil(value) {
  return value == null
}

export function w_p(width, paddingNumber) {
  return (paddingNumber / 375) * width
}

export function w_p21(width) {
  return (21 / 375) * width
}

export function w_p16(width) {
  return (16 / 375) * width
}

export function h_p21(height) {
  return (21 / 667) * height
}

export function h_p16(height) {
  return (16 / 667) * height
}

export function getAppWidth(isInstrusive) {
  let appWidth = width;
  if (isTablet) {
    appWidth = isInstrusive ? 768 : 375;
  }
  return appWidth;
}

export function getAppHeight(isInstrusive) {
  let appHeight = height;
  if (isTablet) {
    appHeight = isInstrusive ? (height * 3) / 4 : 450;
  }
  return appHeight;
}

export const getUniqueID = async () => {
  if (DeviceInfo.getUniqueID) return typeof DeviceInfo.getUniqueID() === 'object' ? await DeviceInfo.getUniqueID() : DeviceInfo.getUniqueID();
  return typeof DeviceInfo.getUniqueId() === 'object' ? await DeviceInfo.getUniqueId() : DeviceInfo.getUniqueId();
}

export const getSystemVersion = async () => {
  return typeof DeviceInfo.getSystemVersion() === 'object' ? await DeviceInfo.getSystemVersion() : DeviceInfo.getSystemVersion();
}

export const getVersion = async () => {
  return typeof DeviceInfo.getVersion() === 'object' ? await DeviceInfo.getVersion() : DeviceInfo.getVersion();
}

export const getBuildNumber = async () => {
  return typeof DeviceInfo.getBuildNumber() === 'object' ? await DeviceInfo.getBuildNumber() : DeviceInfo.getBuildNumber();
}

export const getModel = async () => {
  return typeof DeviceInfo.getModel() === 'object' ? await DeviceInfo.getModel() : DeviceInfo.getModel();
}

export const getBundleId = async () => {
  return typeof DeviceInfo.getBundleId() === 'object' ? await DeviceInfo.getBundleId() : DeviceInfo.getBundleId();
}

export const getManufacturer = async () => {
  return typeof DeviceInfo.getManufacturer() === 'object' ? await DeviceInfo.getManufacturer() : DeviceInfo.getManufacturer();
}

export const getDeviceType = async () => {
  return typeof DeviceInfo.getDeviceType() === 'object' ? await DeviceInfo.getDeviceType() : DeviceInfo.getDeviceType();
}

export const getDeviceId = async () => {
  return typeof DeviceInfo.getDeviceId() === 'object' ? await DeviceInfo.getDeviceId() : DeviceInfo.getDeviceId();
}

export const getBrand = async () => {
  return typeof DeviceInfo.getBrand() === 'object' ? await DeviceInfo.getBrand() : DeviceInfo.getBrand();
}







