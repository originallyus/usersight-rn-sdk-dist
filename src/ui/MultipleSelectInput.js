import React, { useEffect } from "react";
import { View, StyleSheet, Animated, TouchableOpacity, Text } from "react-native";
import * as f from '../utils/MyFunction'

let delayValue = 10;

const AnimatedTouchableOpacity =
  Animated.createAnimatedComponent(TouchableOpacity);

const MultipleSelectInput = ({
  theme,
  options,
  listActiveSelect,
  handleSelectOption,
  handleMultiSelectOption,
  options_multiselect,
  animated,
  appWidth
}) => {
  const {
    option_bg_color,
    option_border_color,
    option_font,
    option_fontsize,
    option_selected_bg_color,
    option_selected_border_color,
    option_selected_text_color,
    option_text_color,
  } = theme;

  useEffect(() => {
    delayValue = 10;
  }, []);

  return (
    <View style={styles.viewOptionsContainer}>
      {options?.map(({ slug, title }) => {
        let activeSelect = false;
        if (listActiveSelect.indexOf(slug) !== -1) {
          activeSelect = true;
        }

        const borderColor = activeSelect
          ? option_selected_border_color
          : option_border_color;

        const backgroundColor = activeSelect
          ? option_selected_bg_color
          : option_bg_color;

        const color = activeSelect
          ? option_selected_text_color
          : option_text_color;

        return (
          <Animated.View
            key={`option-${slug}`}
          >
            <AnimatedTouchableOpacity
              onPress={() =>
                options_multiselect
                  ? handleMultiSelectOption(slug)
                  : handleSelectOption(slug)
              }
              style={[
                styles.btnSelectContainer,
                {
                  borderColor: borderColor,
                  backgroundColor: backgroundColor,
                  borderRadius: f.w_p(appWidth, 16),
                  paddingVertical: 6,
                  paddingHorizontal: 16,
                  marginHorizontal: 6,
                  marginVertical: 8,
                },
              ]}
            >
              <Text
                style={{
                  fontFamily: option_font,
                  fontSize: option_fontsize,
                  textAlign: "center",
                  color,
                }}
              >
                {title}
              </Text>
            </AnimatedTouchableOpacity>
          </Animated.View>
        );
      })}
    </View>
  );
};

MultipleSelectInput.defaultProps = {
  options: [],
};

export default MultipleSelectInput;

const styles = StyleSheet.create({
  btnSelectContainer: {
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  viewOptionsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
  },
});
