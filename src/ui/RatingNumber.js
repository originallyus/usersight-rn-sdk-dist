import React, { useEffect, useRef } from "react";
import { View, StyleSheet, TouchableOpacity, Animated, Text } from "react-native";
import * as f from '../utils/MyFunction'

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

const RatingNumber = (props) => {
  const {
    scale_label_color,
    scale_label_left,
    scale_label_right,
    scale_label_font,
    scale_label_fontsize,
    handlePressRating,
    numStar,
    rating_min,
    rating_max,
    appWidth
  } = props;

  const renderRating = () => {
    const listStar = [];
    for (let i = rating_min; i <= rating_max; i++) {
      listStar.push(
        <ButtonNumber
          key={`rating-number-${i}`}
          {...props}
          handlePressRating={() => {
            handlePressRating(i);
          }}
          number={i}
          active={numStar === i}
          isStart={i === rating_min}
          isEnd={i === rating_max}
        />
      );
    }
    return listStar;
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.ratingContainer}>{renderRating()}</View>
      <View 
        style={{
          ...styles.ratingLabelContainer,
          marginTop: f.w_p(appWidth, 12)
        }}
      >
        <Text
          style={{
            color: scale_label_color,
            fontSize: scale_label_fontsize,
            fontFamily: scale_label_font,
          }}
        >
          {scale_label_left || "Strongly disagree"}
        </Text>
        <Text
          style={{
            color: scale_label_color,
            fontSize: scale_label_fontsize,
            fontFamily: scale_label_font,
          }}
        >
          {scale_label_right || "Strongly agree"}
        </Text>
      </View>
    </View>
  );
};

const ButtonNumber = ({
  handlePressRating,
  active,
  isStart,
  isEnd,
  number,
  rating_number_font,
  rating_number_fontsize,
  rating_number_bg_color,
  rating_number_border_color,
  rating_number_selected_bg_color,
  rating_number_selected_border_color,
  rating_number_selected_text_color,
  rating_number_text_color,
}) => {
  const springAnimated = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    if (active) {
      Animated.timing(springAnimated, {
        toValue: 1,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(springAnimated, {
        toValue: 0,
        useNativeDriver: false,
      }).start();
    }
  }, [active]);

  const backgroundColorAni = springAnimated.interpolate({
    inputRange: [0, 1],
    outputRange: [rating_number_bg_color, rating_number_selected_bg_color],
  });

  return (
    <AnimatedTouchable
      style={[
        styles.buttonNumberContainer,
        {
          borderColor: active
            ? rating_number_selected_border_color
            : rating_number_border_color,
          borderRightWidth: isEnd ? 2 * StyleSheet.hairlineWidth : 0,
          borderTopLeftRadius: isStart ? 8 : 0,
          borderBottomLeftRadius: isStart ? 8 : 0,
          borderTopRightRadius: isEnd ? 8 : 0,
          borderBottomRightRadius: isEnd ? 8 : 0,
          backgroundColor: backgroundColorAni,
        },
      ]}
      onPress={handlePressRating}
      activeOpacity={1}
    >
      <Text
        style={{
          color: active
            ? rating_number_selected_text_color
            : rating_number_text_color,
          fontSize: rating_number_fontsize,
          fontFamily: rating_number_font,
        }}
      >
        {number}
      </Text>
    </AnimatedTouchable>
  );
};

export default RatingNumber;

const styles = StyleSheet.create({
  ratingContainer: {
    flexDirection: "row",
    flex: 1,
  },
  ratingLabelContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  buttonNumberContainer: {
    flex: 1,
    paddingVertical: 12,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
  },
});
