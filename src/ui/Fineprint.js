import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {isTablet, mainPaddingHorizontal, widthScreen} from '../utils/const';

const Fineprint = ({
  fineprint_color,
  fineprint_font,
  fineprint_fontsize,
  fineprint,
  nonIntrusive = false,
  alignment = 'center',
}) => {
  let paddingHorizontal = mainPaddingHorizontal;
  if (nonIntrusive && isTablet) paddingHorizontal = 22;

  return (
    <Text
      style={[
        styles.textComment,
        {
          fontSize: fineprint_fontsize,
          fontFamily: fineprint_font,
          color: fineprint_color,
          textAlign: alignment,
        },
      ]}>
      {fineprint}
    </Text>
  );
};

export default Fineprint;

const styles = StyleSheet.create({
  textComment: {
    marginTop: (4 / 375) * widthScreen,
    paddingTop: (4 / 375) * widthScreen,
    paddingBottom: (12 / 375) * widthScreen,
  },
});
