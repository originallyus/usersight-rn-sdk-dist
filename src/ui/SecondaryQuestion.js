import React from 'react';
import { Text } from 'react-native';

const SecondaryQuestion = ({
  secondary_question_color,
  secondary_question_font,
  secondary_question_fontsize,
  secondary_question,
  alignment = 'center',
}) => {
  return (
    <Text
      style={{
        fontFamily: secondary_question_font,
        color: secondary_question_color,
        fontSize: secondary_question_fontsize,
        textAlign: alignment,
      }}>
      {secondary_question}
    </Text>
  );
};

export default SecondaryQuestion;
