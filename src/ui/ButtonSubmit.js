import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ActivityIndicator,
} from "react-native";
import * as f from '../utils/MyFunction'

const ButtonSubmit = (props) => {
  const {
    onPress,
    title,
    disabled = false,
    loading = false,
    button_text_color,
    button_bg_color,
    button_font,
    button_fontsize,
    customStyle,
    appWidth,
    isTabletInstrusive
  } = props;

  return (
    <View
      style={[
        styles.safeAreaViewContainer,
        {
          marginTop: f.w_p(appWidth, 10),
          ...customStyle,
        },
      ]}
    >
      <View style={styles.viewWrap}>
        <TouchableOpacity
          onPress={disabled || loading ? () => null : onPress}
          style={[
            styles.btnContainer,
            {
              width: isTabletInstrusive ? 200 : "100%",
              opacity: disabled ? 0.5 : 1,
              backgroundColor: button_bg_color,
              borderRadius: 8,
              paddingVertical: 12,
            },
          ]}
          disabled={disabled}
        >
          {loading && <ActivityIndicator style={{ position: "absolute" }} color={button_text_color} />}
          <Text
            style={{
              fontSize: button_fontsize,
              color: button_text_color,
              fontFamily: button_font,
              opacity: loading ? 0 : 1,
            }}
          >
            {title}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  safeAreaViewContainer: {
    width: "100%",
  },
  btnContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  viewWrap: {
    justifyContent: "center",
    alignItems: "center",
  }
});

export default ButtonSubmit;
