import React from 'react';
import {Text} from 'react-native';

const Question = ({
  question_color,
  question_font,
  question_fontsize,
  question,
  alignment = 'center',
}) => {
  return (
    <Text
      style={{
        fontFamily: question_font,
        color: question_color,
        fontSize: question_fontsize,
        textAlign: alignment,
      }}>
      {question}
    </Text>
  );
};

export default Question;
