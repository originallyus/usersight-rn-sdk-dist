import React, { useState } from "react";
import { StyleSheet, TextInput } from "react-native";
import {
  isTablet,
} from "../utils/const";
import * as f from '../utils/MyFunction'

const TextArea = ({
  inputValue,
  comment_placeholder,
  handleChangeText,
  textarea_bg_color,
  textarea_border_color,
  textarea_focus_bg_color,
  textarea_focus_border_color,
  textarea_font,
  textarea_fontsize,
  textarea_placeholder_color,
  textarea_text_color,
  appWidth
}) => {
  const [onFocusInput, setOnFocusInput] = useState(false);

  return (
    <TextInput
      placeholder={comment_placeholder}
      placeholderTextColor={textarea_placeholder_color}
      style={[
        styles.viewImproveInput,
        {
          borderWidth: 1,
          padding: f.w_p(appWidth, 8),
          borderRadius: isTablet ? 6 : 4,
          backgroundColor: onFocusInput
            ? textarea_focus_bg_color
            : textarea_bg_color,
          fontFamily: textarea_font,
          color: textarea_text_color,
          borderColor: onFocusInput
            ? textarea_focus_border_color
            : textarea_border_color,
          fontSize: textarea_fontsize,
        },
      ]}
      multiline
      value={inputValue}
      onChangeText={handleChangeText}
      onFocus={() => setOnFocusInput(true)}
      onBlur={() => setOnFocusInput(false)}
      autoCorrect={false}
    />
  );
};

export default TextArea;

const styles = StyleSheet.create({
  viewImproveInput: {
    flex: 1,
    textAlignVertical: "top",
  },
});
