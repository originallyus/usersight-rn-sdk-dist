import React, { memo, useRef } from "react";
import { TouchableOpacity, Animated, Image } from "react-native";
import { isTablet } from "../utils/const";
import { imgStarAsset } from "../assets";

// milliseconds
let pressRatingTimestamp = null;

const RatingStar = ({
  rating_min,
  rating_max,
  numStar,
  handlePressRating,
  normal_star_color,
  selected_star_color,
  disable = false,
}) => {
  const renderAllStars = () => {
    const listStar = [];
    for (let i = rating_min; i <= rating_max; i++) {
      listStar.push(
        <ButtonStar
          key={`ButtonStar-${i}`}
          normal_star_color={normal_star_color}
          selected_star_color={selected_star_color}
          disabled={disable || numStar === i}
          handlePressRating={() => {
            handlePressRating(i);
          }}
          active={i <= numStar}
        />
      );
    }
    return listStar;
  };

  return <>{renderAllStars()}</>;
};

export default RatingStar;

const ButtonStar = memo((props) => {
  const {
    disabled,
    handlePressRating,
    active = false,
    normal_star_color,
    selected_star_color,
  } = props;
  const springAnimated = useRef(new Animated.Value(0)).current;

  const onPressStar = () => {
    if (pressRatingTimestamp) {
      const nowTimestamp = Math.floor(Date.now());
      if (nowTimestamp - pressRatingTimestamp < 500) return;
    }
    pressRatingTimestamp = Math.floor(Date.now());

    Animated.sequence([
      Animated.spring(springAnimated, {
        toValue: 1,
        tension: 50,
        useNativeDriver: true,
      }),
      Animated.spring(springAnimated, {
        toValue: 0,
        tension: 50,
        useNativeDriver: true,
      }),
    ]).start();
    handlePressRating();
  };

  const scaleStar = springAnimated.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 1.5],
  });

  const tintColor = active ? selected_star_color : normal_star_color;

  return (
    <Animated.View
      style={{
        transform: [
          {
            scale: scaleStar,
          },
        ],
      }}
    >
      <TouchableOpacity disabled={disabled} onPress={onPressStar}>
        <Image
          source={imgStarAsset}
          style={[
            {
              width: isTablet ? 42 : 34,
              height: isTablet ? 42 : 34,
              marginHorizontal: isTablet ? 12 : 8,
              tintColor,
            },
          ]}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </Animated.View>
  );
});
