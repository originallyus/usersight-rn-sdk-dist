import React from 'react';
import { Text } from 'react-native';

const Instruction = ({
  instruction_color,
  instruction_font,
  instruction_fontsize,
  instruction,
  alignment = 'center',
}) => {
  return (
    <Text
      style={{
        fontSize: instruction_fontsize,
        fontFamily: instruction_font,
        color: instruction_color,
        textAlign: alignment,
      }}>
      {instruction}
    </Text>
  );
};

export default Instruction;
