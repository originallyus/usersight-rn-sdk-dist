import React from 'react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Text
} from 'react-native';
import {imgCloseAsset} from '../assets';
import * as f from '../utils/MyFunction'

export const Header = ({
  title,
  title_color,
  title_font,
  title_fontsize,
  image_file_url,
  handleButtonClose,
  alignment = 'center',
  appWidth,
  isTabletInstrusive
}) => {
  return (
    <SafeAreaView>
      <View style={styles.headerContainer}>
        <TouchableOpacity
          hitSlop={styles.hitSlop}
          onPress={handleButtonClose}
          style={{
            ...styles.btnCloseHeader,
            marginBottom: isTabletInstrusive ? 0 : f.w_p(appWidth, 10)
          }}
        >
          <Image
            source={imgCloseAsset}
            style={{
              width: isTabletInstrusive ? 28 : f.w_p(appWidth, 24),
              height: isTabletInstrusive ? 28 :f.w_p(appWidth, 24),
              tintColor: title_color,
            }}
            resizeMode="contain"
          />
        </TouchableOpacity>
        {
          image_file_url && (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image
                source={{uri: image_file_url}}
                style={{
                  width: isTabletInstrusive ? 244 : f.w_p(appWidth, 200),
                  height: isTabletInstrusive ? 244 : f.w_p(appWidth, 200),
                }}
              />
            </View>
          )
        }
        <Text
          style={[
            {
              textAlign: alignment,
              fontFamily: title_font,
              color: title_color,
              fontSize: title_fontsize,
            },
          ]}>
          {title || 'Help us get better'}
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'column',
  },
  hitSlop: {
    top: 10,
    left: 10,
    right: 10,
    bottom: 10,
  },
  btnCloseHeader: {
    marginLeft: 'auto',
  },
});
