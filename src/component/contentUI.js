import React, { useState, useEffect, useRef } from "react";
import {
  View,
  ScrollView,
  StyleSheet,
  Animated,
  LayoutAnimation,
  UIManager,
  Platform,
  Dimensions,
  StatusBar,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { Header } from "./";
import {
  shuffle, isTablet
} from "../utils/const";
import {
  MultipleSelectInput,
  TextArea,
  Fineprint,
  Instruction,
  ButtonSubmit,
  RatingStar,
  RatingNumber,
  Question,
  SecondaryQuestion,
} from "../ui";
import * as f from '../utils/MyFunction'

const heightToLowUI = [
  "question",
  "show_rating",
  "instruction",
  "options",
  "secondary_question",
  "show_comment",
  "fineprint",
  "submit_btn",
];

const NonInstrusiveUI = ({
  didSubmitted,
  form,
  animatedValues,
  animations,
  states,
  functions,
}) => {
  const {
    theme,
    question,
    title,
    options,
    options_multiselect,
    instruction,
    comment_placeholder,
    fineprint,
    button_text,
    rating_max,
    rating_min,
    show_rating_star,
    show_rating_number,
    read_only_rating,
    show_comment,
    comment_mandatory,
    secondary_question,
    image_file_url,
    instrusive_style,
    alignment,
    auto_submit,
  } = form;
  const {
    submitForm,
    handleButtonClose,
    handlePressRating,
    handleChangeText,
    handleSelectOption,
    handleMultiSelectOption,
    scrollViewInputRef,
  } = functions;
  const { animaginSelectBtn, animationInSideApp, } = animations;
  const { numStar, inputValue, listActiveSelect, loading, isShowingExtraForm } = states;
  const { keyboardHeight } = animatedValues;

  const multipleSelectAni = useRef(new Animated.Value(0)).current;
  const secondaryQuestionAni = useRef(new Animated.Value(0)).current;
  const textAreaAni = useRef(new Animated.Value(0)).current;
  const fineprintAni = useRef(new Animated.Value(0)).current;
  const submitBtnAni = useRef(new Animated.Value(0)).current;
  const [showHeightToLowUI, setShowHeightToLowUI] = useState(
    new Array(heightToLowUI.length).fill(false)
  );
  const [showSlug, setShowSlug] = useState("");

  /***********************************************************
   * Handle show UI
   ***********************************************************/
  useEffect(() => {
    if (
      Platform.OS === "android" &&
      UIManager.setLayoutAnimationEnabledExperimental
    ) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    let showIndex = -1;
    if (show_rating_number || show_rating_star || auto_submit) {
      showIndex = heightToLowUI.findIndex((value) => value === "show_rating");
      setShowSlug("show_rating");
    } else if (options?.length !== 0 && options) {
      showIndex = heightToLowUI.findIndex((value) => value === "options");
      setShowSlug("options");
    } else if (show_comment) {
      setShowSlug("show_comment");
    }

    if (showIndex !== -1) {
      const showUI = new Array(showIndex + 1)
        .fill(true)
        .concat(new Array(heightToLowUI.length - showIndex + 1).fill(false));
      setShowHeightToLowUI([...showUI]);
      return;
    }

    // show all
    const showUI = new Array(heightToLowUI.length).fill(true);
    setShowHeightToLowUI([...showUI]);
  }, []);

  useEffect(() => {
    setTimeout(() => {
      if (options?.length !== 0 && options && showHeightToLowUI[3]) {
        Animated.timing(multipleSelectAni, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }

      if (secondary_question && showHeightToLowUI[4]) {
        Animated.timing(secondaryQuestionAni, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }

      if (show_comment && showHeightToLowUI[5]) {
        Animated.timing(textAreaAni, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }

      if (fineprint && showHeightToLowUI[6]) {
        Animated.timing(fineprintAni, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }

      if (showHeightToLowUI[7]) {
        Animated.timing(submitBtnAni, {
          toValue: 1,
          useNativeDriver: false,
        }).start();
      }
    }, 300);
  }, [showHeightToLowUI]);

  useEffect(() => {
    if (auto_submit?.includes(numStar)) return;

    if (numStar !== 0 && showSlug === "show_rating") {
      const showUI = new Array(heightToLowUI.length).fill(true);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      setShowHeightToLowUI([...showUI]);
    }
  }, [numStar]);

  useEffect(() => {
    if (listActiveSelect.length > 0 && showSlug === "options") {
      const showUI = new Array(heightToLowUI.length).fill(true);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      setShowHeightToLowUI([...showUI]);
    }
  }, [listActiveSelect]);

  useEffect(() => {
    if (didSubmitted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

      //show everything
      const showUI = new Array(heightToLowUI.length).fill(true);
      setShowHeightToLowUI([...showUI]);
    }
  }, [didSubmitted]);

  /***********************************************************
   * Submit UI
   ***********************************************************/
  const showUI = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    if (isShowingExtraForm) {
      Animated.sequence([
        Animated.timing(showUI, {
          toValue: 0,
          duration: 350,
          useNativeDriver: false,
        }),
        Animated.timing(showUI, {
          toValue: 1,
          duration: 350,
          useNativeDriver: false,
        }),
      ]).start(() => {});
    }
  }, [isShowingExtraForm]);

  /************************************************************/

  const opacitySecondaryQuestion = secondaryQuestionAni.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });

  const opacityTextArea = textAreaAni.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });

  const opacityFineprint = fineprintAni.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });

  const opacitySubmitBtn = submitBtnAni.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });

  let disableSumitBtn = false;
  if (comment_mandatory == 1 && !inputValue.trim()) {
    disableSumitBtn = true;
  }

  if (show_rating_star == 1 || show_rating_number == 1)
    if (numStar == 0) disableSumitBtn = true;

  // Config UI
  const appWidth = f.getAppWidth(instrusive_style);
  let isTabletInstrusive = false
  if (isTablet && !!instrusive_style)
    isTabletInstrusive = true
  //================================================

  return (
    <>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{
          paddingHorizontal: isTabletInstrusive ? 22 : f.w_p21(appWidth),
          paddingTop: isTabletInstrusive ? 22 : f.w_p16(appWidth),
          marginTop: Platform.OS === "android" && !isTablet && !!instrusive_style ? StatusBar.currentHeight : 0
        }}>
          <Header
            {...theme}
            handleButtonClose={handleButtonClose}
            title={title}
            image_file_url={image_file_url}
            instrusive_style={instrusive_style}
            alignment={alignment}
            appWidth={appWidth}
            isTabletInstrusive={isTabletInstrusive}
          />
        </View>
      </TouchableWithoutFeedback>
      <ScrollView
        horizontal
        pagingEnabled
        style={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
        scrollEnabled={false}
        keyboardShouldPersistTaps="always"
      >
        <View
          style={[
            styles.ratingContainer,
            {
              width: appWidth,
              //paddingHorizontal: isTabletInstrusive ? 80 : f.w_p21(appWidth),
              marginTop: isTabletInstrusive ? 22 : f.w_p16(appWidth),
            },
          ]}
        >
          <ScrollView
            ref={(ref) => scrollViewInputRef(ref)}
            style={{ flex: 1, width: "100%" }}
            contentContainerStyle={{ 
              flexGrow: 1, 
              alignItems: "center",
              paddingHorizontal: isTabletInstrusive ? 80 : f.w_p21(appWidth),
            }}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            // keyboardShouldPersistTaps="always"
          >
            {/* Question */}
            {question && showHeightToLowUI[0] && (
              <Animated.View
                style={{
                  width: "100%",
                  marginBottom: isTabletInstrusive ? 20 : f.w_p16(appWidth),
                }}
              >
                <Question
                  {...theme}
                  question={question}
                  alignment={alignment}
                />
              </Animated.View>
            )}

            {(show_rating_number == 1 || show_rating_star == 1) &&
              showHeightToLowUI[1] && (
                <View 
                  style={{
                    flexDirection: "row",
                    marginBottom: isTabletInstrusive ? 20 : f.w_p16(appWidth),
                  }}
                >
                  {/* RatingNumber */}
                  {show_rating_number == 1 && (
                    <RatingNumber
                      {...theme}
                      numStar={numStar}
                      rating_min={rating_min}
                      rating_max={rating_max}
                      handlePressRating={
                        isShowingExtraForm ? () => null : handlePressRating
                      }
                      appWidth={appWidth}
                    />
                  )}

                  {/* RatingStar */}
                  {show_rating_star == 1 && (
                    <RatingStar
                      {...theme}
                      numStar={numStar}
                      rating_min={rating_min}
                      rating_max={rating_max}
                      handlePressRating={
                        isShowingExtraForm ? () => null : handlePressRating
                      }
                      disable={read_only_rating||false}
                    />
                  )}
                </View>
              )}

            {/* Instruction */}
            {((instruction && showHeightToLowUI[2]) || isShowingExtraForm) && (
              <Animated.View
                style={{
                  width: "100%",
                  marginBottom: isTabletInstrusive ? 20 : f.w_p16(appWidth),
                }}
              >
                <Instruction
                  {...theme}
                  instruction={
                    form[`instruction_${numStar}_star`] || instruction
                  }
                  alignment={alignment}
                />
              </Animated.View>
            )}

            {/* MultipleSelectInput */}
            {options?.length !== 0 && options && showHeightToLowUI[3] && (
              <Animated.View
                style={{
                  width: "100%",
                  marginBottom: isTabletInstrusive ? 20 : f.w_p16(appWidth),
                }}
              >
                <MultipleSelectInput
                  theme={theme}
                  options={options}
                  animaginSelectBtn={animaginSelectBtn}
                  listActiveSelect={listActiveSelect}
                  handleSelectOption={handleSelectOption}
                  handleMultiSelectOption={handleMultiSelectOption}
                  options_multiselect={options_multiselect}
                  animated={multipleSelectAni}
                  appWidth={appWidth}
                />
              </Animated.View>
            )}

            {/* SecondaryQuestion */}
            {secondary_question && showHeightToLowUI[4] && (
              <Animated.View
                style={{
                  width: "100%",
                  opacity: opacitySecondaryQuestion,
                  marginBottom: isTabletInstrusive ? 20 : f.w_p16(appWidth),
                }}
              >
                <SecondaryQuestion
                  {...theme}
                  secondary_question={
                    form[`secondary_question_${numStar}_star`] ||
                    secondary_question
                  }
                  alignment={alignment}
                />
              </Animated.View>
            )}

            {/* TextArea */}
            {show_comment && showHeightToLowUI[5] && (
              <Animated.View
                style={{
                  opacity: opacityTextArea,
                  width: "100%",
                  height: isTablet ? 110 : 94,
                  marginBottom: isTabletInstrusive ? 20 : f.w_p16(appWidth),
                }}
              >
                <TextArea
                  {...theme}
                  inputValue={inputValue}
                  numStar={numStar}
                  handleChangeText={handleChangeText}
                  comment_placeholder={
                    form[`comment_placeholder_${numStar}_star`] ||
                    comment_placeholder
                  }
                  animated={textAreaAni}
                  appWidth={appWidth}
                />
              </Animated.View>
            )}

            {/* Fineprint */}
            {fineprint && showHeightToLowUI[6] && (
              <Animated.View
                style={{
                  width: "100%",
                  opacity: opacityFineprint,
                }}
              >
                <Fineprint
                  {...theme}
                  fineprint={fineprint}
                  alignment={alignment}
                />
              </Animated.View>
            )}
          </ScrollView>
          {(showHeightToLowUI[7] || isShowingExtraForm) && (
            <Animated.View
              style={{
                width: "100%",
                opacity: opacitySubmitBtn,
                paddingHorizontal: isTabletInstrusive ? 80 : f.w_p21(appWidth),
              }}
            >
              <ButtonSubmit
                {...theme}
                title={button_text}
                onPress={() => submitForm()}
                disabled={disableSumitBtn}
                loading={loading}
                appWidth={appWidth}
                isTabletInstrusive={isTabletInstrusive}
              />
            </Animated.View>
          )}
          
          <Animated.View
            style={{
              paddingBottom: animationInSideApp,
              width: appWidth,
            }}
          />
        </View>
      </ScrollView>
    </>
  );
};

export default NonInstrusiveUI;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  ratingContainer: {
    flex: 1,
    alignItems: "center",
  },
});
