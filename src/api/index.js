import { myAxiosPost } from "../utils/NetworkHelper";
import { Alert } from "react-native";

export const sendRequest = async (bodyData, options = {}) => {
  try {
    const response = await myAxiosPost("/8vs2qwFD1rlSFWIa13Wc", bodyData, options); // sdk/request_form

    // sanity checks
    if (!response) {
      console.log("UserSightSDK Error: response is empty. Code 9370");
      return null;
    }
    if (!response.data) {
      console.log("UserSightSDK Error: response.data is empty. Code 9373");
      return response.data
    }

    // log to console, if there is 'debug_message'
    if (response.data.debug_message) {
      console.log("UserSightSDK Debug:", response.data.debug_message);
    }

    // critical error, show directly to UI
    if (response.data.error) {
      Alert.alert("UserSightSDK Error", response.data.error, [{ text: "OK" }], {
        cancelable: true,
      });
      return response;
    }
    return response.data;

  } catch (error) {
    console.log("UserSightSDK Error: request form error. Code 7451", error);
  }
};

export const sendSubmit = async (bodyData, options = {}) => {
  try {
    const response = await myAxiosPost(`/LXvcFSTTOqxxmJX9qQar`, bodyData, options); // sdk/submit_rating

    // sanity checks
    if (!response) {
      console.log("UserSightSDK Error: response is empty. Code 2370");
      return null;
    }
    if (!response.data) {
      console.log("UserSightSDK Error: response.data is empty. Code 2373");
      return response.data
    }

    // critical error, show directly to UI
    if (response.data.error) {
      Alert.alert("UserSightSDK Error", response.data.error, [{ text: "OK" }], {
        cancelable: true,
      });
      return response;
    }
    return response.data;
  } catch (error) {
    console.log("UserSightSDK Error: request form error. Code 8417", error);
  }
};
